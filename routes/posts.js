const router = require("express").Router();

const {
  allPosts,
  addPost,
  postById,
  updatePost,
  deletePost,
} = require("../controllers/postsControllers");

router.get("/", allPosts);

router.get("/:id", postById);

router.post("/", addPost);

router.patch("/:id", updatePost);

router.delete("/:id", deletePost);

module.exports = router;
