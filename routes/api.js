const router = require("express").Router();

const postRouter = require("./posts");

router.use("/posts", postRouter);

router.get("/", (req, res) => {
  res.send("Blog API");
});

module.exports = router;
