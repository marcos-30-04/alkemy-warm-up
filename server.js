const express = require("express");
const app = express();

const port = process.env.PORT || 5000;

require("./database/db");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const apiRouter = require("./routes/api");

app.use("/", apiRouter);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
