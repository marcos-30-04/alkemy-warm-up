const { Post } = require("../database/db");

// GET '/'
const allPosts = (req, res) => {
  Post.findAll({
    order: [["createdAt", "DESC"]],
    include: {
      association: "category",
      attributes: ["title"],
    },
    attributes: ["id", "title", "img", "createdAt"],
  }).then((posts) => {
    res.json(posts);
  });
};

// POST '/'
const addPost = (req, res) => {
  Post.create(req.body)
    .then((post) => {
      res.json(post);
    })
    .catch((err) => res.status(400).json("Error: " + err));
};

// GET '/:id'

const postById = (req, res) => {
  Post.findByPk(req.params.id, {
    include: {
      association: "category",
      attributes: ["title"],
    },
    attributes: ["id", "img", "title", "content", "createdAt"],
  })
    .then((post) => {
      !post && res.json({ error: "no such post" });
      res.json(post);
    })
    .catch((err) => res.status(400).json("Error: " + err));
};

// PATCH '/:id'

const updatePost = async (req, res) => {
  // Check if post exists
  Post.findByPk(req.params.id).then(
    (postById) => !postById && res.json({ error: "no such post" })
  );

  // Post found and updated
  Post.update(req.body, {
    where: { id: req.params.id },
  })
    .then(() => {
      res.status(201).json(`You've edited the post successfully!`);
    })
    .catch((err) => {
      res.status(400).json("Error: " + err);
    });
};

// DELETE '/:id'

const deletePost = (req, res) => {
  // Check if post exists
  Post.findByPk(req.params.id).then(
    (postById) => !postById && res.json({ error: "no such post" })
  );
  // Post found and deleted
  Post.destroy({ where: { id: req.params.id } })
    .then(() => {
      res.status(200).json("Post deleted successfully.");
    })
    .catch((err) => {
      res.status(400).json("Error: " + err);
    });
};

module.exports = {
  allPosts,
  addPost,
  postById,
  updatePost,
  deletePost,
};
