const Sequelize = require("sequelize");
const { database } = require("./config");
const PostModel = require("./models/Post");
const CategoryModel = require("./models/Category");

const sequelize = new Sequelize(
  database.database,
  database.username,
  database.password,
  {
    host: database.host,
    dialect: "mysql",
  }
);

const Post = PostModel(sequelize, Sequelize);
const Category = CategoryModel(sequelize, Sequelize);

Category.hasMany(Post);
Post.belongsTo(Category);

sequelize.sync({ force: false }).then(() => {
  console.log("Tables Synchronized");
});

module.exports = { Post, Category };
