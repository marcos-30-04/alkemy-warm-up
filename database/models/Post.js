module.exports = (sequelize, type) => {
  return sequelize.define("post", {
    id: {
      type: type.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: type.TEXT,
    content: type.TEXT,
    img: {
      type: type.TEXT,
      validate: {
        is: /(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/g,
      },
    },
  });
};
